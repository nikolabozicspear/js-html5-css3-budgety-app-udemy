var budgetController = (function() {

    var Expense = function(id, description, value) {
        this.id = id;
        this.description = description;
        this.value = value;
        this.percentage = -1;        
    };

    Expense.prototype.calcPercentage = function(totalIncome) {
        if(totalIncome > 0) {
            this.percentage = Math.round((this.value/totalIncome) * 100);
        } else {
            this.percentage = -1;
        }
        
    };

    Expense.prototype.getPercentage = function() {
        return this.percentage;
    };
    
    var Income = function(id, description, value) {
        this.id = id;
        this.description = description;
        this.value = value;
    }
    
    var data = {
        allItems: {
            income:[],
            expense:[]
        },
        totals: {
            income:0,
            expense:0
        },
        budget:0,
        percentage:-1
    };

    var calculateTotal = function(type) {
        var sum = 0;

        data.allItems[type].forEach(function(current) {
            sum += current.value;            
        });     
          
        data.totals[type] = sum; 
    }

    return { 
        addItem: function(type, des, val) {  
            if(data.allItems[type].length > 0) {
                ID = data.allItems[type][data.allItems[type].length -1].id +1;
            } else {
                ID = 0;
            }
         
            var newItem = type === 'expense'? new Expense(ID, des, val) :
            new Income (ID, des, val);  
            data.allItems[type].push(newItem);
            return newItem;
        },

        deleteItem: function(type, id) {
            ids = data.allItems[type].map(function(current) {/* for each making new array */
                return current.id;
            });            

            index = ids.indexOf(id);

            if(index !== -1) {
                data.allItems[type].splice(index, 1);
            }
        },

        calculateBudet:function() {            
            calculateTotal('expense');
            calculateTotal('income');       
          
            data.budget = data.totals.income - data.totals.expense;
            data.percentage = Math.round((data.totals.expense / data.totals.income) * 100);
        },

        calculatePercentages:function() {       
            data.allItems.expense.forEach(function(current) {
                current.calcPercentage(data.totals.income)
            });    
        },

        getPercentages:function() {
            var allPerc = data.allItems.expense.map(function(current) {
                return current.getPercentage();
            }); 
            
            return allPerc;
        },

        getBudget: function() {
            return {
                budget: data.budget,
                totalIncome: data.totals.income,
                totalExpense: data.totals.expense,
                percentage: data.percentage
            }
        },

        testing: function() {
            console.log(data);
        }
    }

})();

var UIController = (function() {

    var  DOMstrings = {
        inputType:'.add__type',
        descriptionType:'.add__description',
        valueType:'.add__value',
        addBtn:'.add__btn',
        incomeContainer: '.income__list',
        expensesContainer: '.expenses__list',
        budgetLabel: '.budget__value',
        incomeLabel: '.budget__income--value',
        expensesLabel: '.budget__expenses--value',
        percentageLabel: '.budget__expenses--percentage',
        container: '.container',
        expesesPercLabel:'.item__percentage',
        dateLabel:'.budget__title--month'
    }

    var nodeListForEach = function(list, callback) {
        for(var i = 0; i < list.length; i++) {
            callback(list[i], i);
        }
    }

    return {
        getinput: function() {     
            return {
              type:document.querySelector(DOMstrings.inputType).value,
              description:document.querySelector(DOMstrings.descriptionType).value,
              value: parseFloat(document.querySelector(DOMstrings.valueType).value)
            }            
        },

        addListItem: function(obj, type) {
            var html, newHtml, element;

            if(type === 'income') {
                element = DOMstrings.incomeContainer;
                html = '<div class="item clearfix" id="income-%id%"> <div class="item__description">%description%</div><div class="right clearfix"><div class="item__value">%value%</div><div class="item__delete"><button class="item__delete--btn"><i class="ion-ios-close-outline"></i></button></div></div></div>';
            } else if(type === 'expense') {
                element = DOMstrings.expensesContainer;
                html = '<div class="item clearfix" id="expense-%id%"><div class="item__description">%description%</div><div class="right clearfix"><div class="item__value">%value%</div><div class="item__percentage">21%</div><div class="item__delete"><button class="item__delete--btn"><i class="ion-ios-close-outline"></i></button></div></div></div>';
            }     
            
            // Replace the placeholder text with some actual data
            newHtml = html.replace('%id%', obj.id);
            newHtml = newHtml.replace('%description%', obj.description);
            newHtml = newHtml.replace('%value%', obj.value);
            
            // Insert the HTML into the DOM
            document.querySelector(element).insertAdjacentHTML('beforeend', newHtml);
        },     
        
        deleteListItem:function(selectorId) {
            var el = document.getElementById(selectorId);
            el.parentNode.removeChild(el);
        },        

        clearfields: function() {
            var fields, fieldsArr;

            fields = document.querySelectorAll(DOMstrings.descriptionType +
                 ', ' + DOMstrings.valueType);

            fieldsArr = Array.prototype.slice.call(fields);
            fieldsArr.forEach(function(current, index, array){
                current.value = "";
            });            
        },

        displayBudet: function(obj) {
            document.querySelector(DOMstrings.budgetLabel).textContent = obj.budget;
            document.querySelector(DOMstrings.incomeLabel).textContent = obj.totalIncome;
            document.querySelector(DOMstrings.expensesLabel).textContent = obj.totalExpense;

            if (obj.percentage > 0) {
                document.querySelector(DOMstrings.percentageLabel).textContent = obj.percentage + '%';
            } else {
                document.querySelector(DOMstrings.percentageLabel).textContent = '---';
            }
        },

        displayPercentages:function(percentages) {
            var fields = document.querySelectorAll(DOMstrings.expesesPercLabel);
            
            nodeListForEach(fields, function(current, index) {

                if(percentages[index] > 0) {
                    current.textContent = percentages[index] + '%';
                } else {
                    current.textContent = percentages[index] + '---';
                } 
            });
        },

        displayMonth: function() {
            var now = new Date();

            year = now.getFullYear();
            month = now.getMonth();

            document.querySelector(DOMstrings.dateLabel).textContent = year + ' ' + month;
        },

        changeType:function() {
            var fields = document.querySelectorAll(
                DOMstrings.inputType + ',' +
                DOMstrings.descriptionType + ',' +
                DOMstrings.valueType);

            nodeListForEach(fields, function(current) {
                current.classList.toggle('red-focus');             
            });

            document.querySelector(DOMstrings.addBtn).classList.toggle('red');

        },
        
        getDOMStrings: function() {
            return DOMstrings;
        }        
    }

})();

var controller = (function(budgetCtrl, UICtrl) {

    var setupEventListeners = function() {
        var DOM = UICtrl.getDOMStrings();

        document.querySelector(DOM.addBtn).addEventListener('click',ctrlAddItem);
        document.addEventListener('keypress', function(event) {      
            if(event.keyKode === 13 || event.which === 13) {
                ctrlAddItem();
            }
        });  
        document.querySelector(DOM.container).addEventListener('click', ctrDeleteItem);     
        document.querySelector(DOM.inputType).addEventListener('change', UICtrl.changeType);     

    }

    var updateBudget = function() {
        budgetCtrl.calculateBudet();
        var budget = budgetCtrl.getBudget();

        UICtrl.displayBudet(budget);      
    }

    var updatePercentages = function() {
        budgetController.calculatePercentages();

        var percentages = budgetController.getPercentages();

         UIController.displayPercentages(percentages);
    }
     
    var ctrlAddItem = function() {
        var input, newItem;

        input = UICtrl.getinput();

        if(input.description !== "" && !isNaN(input.value) && input.value > 0) {
            newItem = budgetController.addItem
            (input.type, input.description, input.value);

            UICtrl.addListItem(newItem, input.type);
            UICtrl.clearfields();

            updateBudget();
            updatePercentages();
        }
    };

    var ctrDeleteItem = function(event) {
        var itemId, splitID, type, id;

        itemId = (event.target.parentNode.parentNode.parentNode.parentNode.id);

        if(itemId) {
            splitID = itemId.split('-');
            type = splitID[0];
            id =  parseInt(splitID[1]);
        }

        budgetController.deleteItem(type, id);
        UIController.deleteListItem(itemId);

        updateBudget();
        updatePercentages();
    };

    return {
        init:function() {
            console.log('Application has started');     
            
            UIController.displayMonth();
          
            UICtrl.displayBudet({
                budget: 0,
                totalInc: 0,
                totalExp: 0,
                percentage: -1
            });

            setupEventListeners();
          
        }
    } 

})(budgetController, UIController);

controller.init();




